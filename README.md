## Eita Ceverzas

## Pasos

- 1)Clona el proyecto.
- 2)Abrí el directorio del proyecto en tu editor de código
- 3)Corré el comando npm install para instalar y actualizar las dependencias.
- 4)Corré el comando npm start para inicializarlo.
- 5)Abrí http://localhost:3000 para verlo en tu navegador.
